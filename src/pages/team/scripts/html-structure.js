function getTeamMembersHTML(members, tasks) {
    const html = members.map(item => {
        const userTasks = tasks.filter(task => {
            return task.assignee === item.email;
        });

        return `<div class="members__card flex" data-id="${item.email}">
				<img src="img/userpic-big.jpg" class="members__icon" alt="">
				<div>
					<p class="members__name">${item.name}</p>
					<p class="members__role">${item.role}</p>
				</div>

				<div class="members__tasks">
					<p class="tasks__count">${userTasks.length}</p>
					<p class="plain__text">задач</p>
				</div>
			</div>`
    }).join('');

    return html;
}

function getTaskBlock(tasks) {
    const html = tasks.map(item => {
        return `<a href="#" class="task task__block">
						<span class="checkbox">
							<img src="img/check-icon.svg" alt="">
						</span>
						<div>
							<p class="task__description">${item.title}</p>
							<div class="flex">
								<p class="task__label development">${item.tag}</p>
							</div>
						</div>
					</a>`;
    }).join('');

    return html;
}

export { getTeamMembersHTML, getTaskBlock };
