import '../../../css/style.scss';
import { getLogin, getCredentials, getTasks } from "../../../api";
import { getTeamMembersHTML, getTaskBlock } from './html-structure';

const userProfile = document.querySelector('.user.user__section');
const projectSection = document.querySelector('.projects.section');
const projectList = projectSection.querySelector('.projects__list');
const projectItems = projectList.querySelectorAll('li');
const teamSection = document.querySelector('.team.section');
const teams = teamSection.querySelectorAll('.team__description');
const projectTitle = document.querySelector('.prj__title__block h1');
const teamList = document.querySelector('.members__list-team');
const memberProfile = document.querySelector('.member__profile');
const credentials = getCredentials();
const tasks = getTasks();

(function () {
    const userName = userProfile.querySelector('.user__name');
    const userRole = userProfile.querySelector('.user__role');
    const loginObj = getLogin();
    const activeTeam = teamSection.querySelector('.team__description');
    const dataId = activeTeam.getAttribute('data-id');

    memberProfile.style.display = 'none';

    userName.innerHTML = loginObj.name;
    loginObj.role ? userRole.innerHTML = loginObj.role : userRole.innerHTML = '';

    getActiveTeamMembers(dataId);
})();

userProfile.addEventListener('click', () => {
    window.location.href = 'profile-settings.html';
});

projectItems.forEach(item => {
    item.addEventListener('click', () => {
        window.location.href = 'task-board.html';
    })
});

teams.forEach(item => {
    item.addEventListener('click', () => {
        const dataId = item.getAttribute('data-id');

        memberProfile.style.display = 'none';

        getActiveTeamMembers(dataId);
    })
});

function getActiveTeamMembers(dataId) {
    projectTitle.innerHTML = dataId === 'design' ? 'Дизайнеры' :
        dataId === 'frontend' ?'Фронтэнд' : 'Бекэнд';

    const prevMembers = teamList.querySelectorAll('.members__card');
    prevMembers.forEach(item => {
        item.remove();
    });

    const activeTeam = credentials.filter(item => {
        return item.team === dataId;
    });

    const html = getTeamMembersHTML(activeTeam, tasks);

    teamList.insertAdjacentHTML('afterBegin', html);

    const memberCards = teamList.querySelectorAll('.members__card');
    const count = document.querySelector('.members__count');
    count.innerHTML = memberCards.length > 0 ? memberCards.length : '';

    memberCards.forEach(item => {
        item.addEventListener('click', () => {
            const dataId = item.getAttribute('data-id');
            console.log('click');
            getMoreMemberInfo(dataId);
        })
    });
}

function getMoreMemberInfo(dataId) {
    const subtitleCount = memberProfile.querySelector('.subtitle .count');
    const maxContainer = memberProfile.querySelector('.max__container');
    const name = memberProfile.querySelector('.profile__name');
    const role = memberProfile.querySelector('.profile__role');
    const member = credentials.find(item => dataId === item.email);
    const membersTasks = tasks.filter(item => item.assignee === member.email);
    const doneTasks = membersTasks.filter(item => item.board === 'done');
    const html = getTaskBlock(membersTasks);
    const prevTasks = maxContainer.querySelectorAll('.task__block');
    const totalFinishedTasks = memberProfile.querySelector('.finished-count');
    const totalOpenedTasks = memberProfile.querySelector('.opened-count');

    prevTasks.forEach(item => {
        item.remove();
    });

    memberProfile.style.display = 'block';

    name.innerHTML = member.name;
    role.innerHTML = member.role;
    subtitleCount.innerHTML = membersTasks.length;
    totalFinishedTasks.innerHTML = doneTasks.length;
    totalOpenedTasks.innerHTML = membersTasks.length - doneTasks.length;

    maxContainer.insertAdjacentHTML('afterBegin', html);
}
