import '../../../css/style.scss';
import { getTasks, setTasks, getLogin } from "../../../api";
import { getBoarderTaskHTML, getCommentHTML } from "../../taskBoard/scripts/html-structure";
import { getError, resetError } from "../../index/scripts/common";

const tabNavigation = document.querySelector('.tab__navigation');
const tabNavigationItems = tabNavigation.querySelectorAll('li');
const tabActiveItem = tabNavigation.querySelector('li[data-id="task-description"]');
const tasksColumn = document.querySelector('.tasks__column');
const tasksDescCard  = document.querySelector('.task__desctiption__card');
const submit = tasksDescCard.querySelector('.task__desctiption__comments .comment__submit');
const commentsSection = tasksDescCard.querySelector('.task__desctiption__comments');
const commentsHistory = commentsSection.querySelector('.comment__history');
const boardBacklog = tasksColumn.querySelector('div[data-id="backlog"]');
const boardTodo = tasksColumn.querySelector('div[data-id="todo"]');
const projectSection = document.querySelector('.projects.section');
const projectList = projectSection.querySelector('.projects__list');
const projectItems = projectList.querySelectorAll('li');
const userProfile = document.querySelector('.user.user__section');
const teamSection = document.querySelector('.team.section');
const teams = teamSection.querySelectorAll('.team__description');
let tempTask = {};

teams.forEach(item => {
    item.addEventListener('click', () => {
        window.location.href = 'team.html';
    })
});

userProfile.addEventListener('click', () => {
    window.location.href = 'profile-settings.html';
});

(function () {
    const userName = userProfile.querySelector('.user__name');
    const userRole = userProfile.querySelector('.user__role');
    const loginObj = getLogin();

    userName.innerHTML = loginObj.name;
    loginObj.role ? userRole.innerHTML = loginObj.role : userRole.innerHTML = '';

    renderBoardTasks(boardBacklog);
    renderBoardTasks(boardTodo);

    tabActiveItem.classList.add('active');
    tasksDescCard.style.display = 'none';
})();

projectItems.forEach(item => {
    item.addEventListener('click', () => {
        window.location.href = 'task-board.html';
    })
});

tabNavigationItems.forEach(item => {
    item.addEventListener('click', () => {
        const dataId = item.getAttribute('data-id');

        if(dataId === 'task-board') {
            window.location.href = 'task-board.html';
        } else if(dataId === 'task-description') {
            window.location.href = 'task-description.html';
        } else if (dataId === 'calendar') {
            window.location.href = 'calendar.html';
        }
    })
});

tasksColumn.addEventListener('click', (e) => {
    const target = e.target;
    const isTarget = target.getAttribute('class') === 'task task__block';
    const finished = tasksColumn.querySelector('.task--finished');

    if(isTarget) {
        if(finished) {
            finished.classList.remove('task--finished');
        }

        tempTask = {};
        tasksDescCard.style.display = 'block';
        target.classList.add('task--finished');
        const dataDate = target.getAttribute('data-date');  //date
        const title = target.querySelector('.task__description').innerHTML;

        commentsHistory.innerHTML = '';
        openTask(dataDate, title);
    }
});

function openTask(dataDate, title) {
    const taskArray = getTasks();

    tempTask = taskArray.find(item => {
        return item.date === dataDate && item.title === title;
    });

    completeFullTask(tempTask);
}

function completeFullTask(task) {
    const title = tasksDescCard.querySelector('.item__title');
    const deadline = tasksDescCard.querySelector('.dueon__date');
    const assignee = tasksDescCard.querySelector('.executor__name');
    const tag = tasksDescCard.querySelector('.tag__block .task__label');
    const description = tasksDescCard.querySelector('.task__desctiption__textarea p');
    const commentsArray = task.comments;

    console.log('task', task);
    console.log('task.comments', commentsArray);
    console.log('length', commentsArray.length);

    title.innerHTML = task.title;
    deadline.innerHTML = task.deadline;
    assignee.innerHTML = task.name;
    tag.innerHTML = task.tag;
    description.innerHTML = task.description;

    if(commentsArray.length > 0) {
        const html = getCommentHTML(commentsArray);

        commentsHistory.insertAdjacentHTML('beforeEnd', html);
    }
}

submit.addEventListener('click', () => {
    let comment = addComment(tempTask);

    updatesComments(tempTask, comment);
});

function addComment(task) {
    const input = commentsSection.querySelector('.comment__input');
    const value = input.value;
    const user = getLogin();
    const comment = {
        name: user.name,
        role: user.team,
        person: task.name,
        value: value,
        date: new Date().toLocaleDateString()
    };

    if(value === '') {
        getError(input);
    } else {
        const html = getCommentHTML(comment);
        commentsHistory.insertAdjacentHTML('beforeEnd', html);

        resetError(input);
        input.value = '';

        return comment;
    }
}

function updatesComments(task, comment) {
    const taskArray = getTasks();

    const updatedArray = taskArray.map(item => {
        if(item.date === task.date && item.title === task.title) {
            item.comments.push(comment);
        }

        return item;
    });

    setTasks(updatedArray);
}

function renderBoardTasks(board) {
    const dataId = board.getAttribute('data-id');
    const tasksArray = getTasks();
    const taskFilter = tasksArray.filter(item => item.board === dataId);
    const html = taskFilter.map(item => getBoarderTaskHTML(item)).join('');

    board.insertAdjacentHTML('beforeEnd', html);
}

