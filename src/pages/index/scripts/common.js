function getError(input, attr) {
    input.style.border = '1px solid red';

    if (attr === 'email') {
        console.error('Введите валидный email');

        return false;
    } else if (attr === 'password') {
        console.error('Введите валидный пароль');

        return false;
    } else if (attr === 'text') {
        console.error('Введите валидное имя и фамилию');

        return false;
    } else {
        console.error('Введите валидные данные');

        return false;
    }
}

function resetError(item) {
    item.style.borderColor = '#EAEAEA';
}

function resetValue(...rest) {
    rest.forEach(input => input.value = '');
}

function getDropDown(parent, child) {
    let isOpen = parent.classList.contains('dropdown--open');

    if(isOpen) {
        parent.classList.remove('dropdown--open');
        child.classList.remove('active');
    } else {
        parent.classList.add('dropdown--open');
        child.classList.add('active');
    }
}

export { getError, getDropDown, resetError, resetValue };
