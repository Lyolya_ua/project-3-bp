import '../../../css/style.scss';
import { getError } from './common';
import { getCredentials, setCredentials, setLogin, updateCredentials } from '../../../api/index';

const formRegistration = document.querySelector('.form--registration');
const formLogin = document.querySelector('.form--login');
const formSetup = document.querySelector('.form--setup');
const linkRegistration = formLogin.querySelector('.additional__info-link--registration');
const linkEnter = formRegistration.querySelector('.additional__info-link--enter');
const registBtn = formRegistration.querySelector('.form-submit');
const enterBtn = formLogin.querySelector('.form-submit');
const fowardBtn = formSetup.querySelector('.form-submit');
let user = {};

formRegistration.classList.add('active');

linkEnter.addEventListener('click', () => {
    getActiveBlock(formRegistration, formLogin);
});

linkRegistration.addEventListener('click', () => {
    getActiveBlock(formLogin, formRegistration);
});

registBtn.addEventListener('click', (e) => {
    e.preventDefault();
    user = getFormData();

    if (user) {
        checkedUserInData(user, formRegistration) ? getActiveBlock(formRegistration, formSetup) : null;
    }
});

enterBtn.addEventListener('click', (e) => {
    e.preventDefault();

    user = getFormData();

    if (user) {
        checkedUserInData(user, formLogin) ? getActiveBlock(formLogin, formSetup) : null;
    }
});

fowardBtn.addEventListener('click', (e) => {
    e.preventDefault();
    const inputsTeam = formSetup.querySelectorAll('input[type="radio"]');

    inputsTeam.forEach(input => {
        if(input.checked === true) {
            user.team = input.id;
        }
    });

    if(user.team === undefined) {
        alert('Выберите команду');
    } else {
        checkedUserInData(user, formSetup);
        user = {};

        window.location.href = 'task-board.html';
    }
});

function checkedUserInData (user, form) {
    const credentials = getCredentials();
    const currentForm = form.classList.contains('form--login') ? 'login'
        : form.classList.contains('form--setup') ? 'setup'
            : 'registration';

    if (currentForm === 'login') {
        const find = credentials.find(item => {
            return item.email === user.email && item.password === user.password;
        });

        if (!find) {
            getAlert(form, 'Введите валидные данные');

            return false;
        }

        setLogin(find);

        Object.assign(user, find);

        return find;
    }

    else if (currentForm === 'setup') {
        credentials.forEach(item => {
            if (item.email === user.email) {
                item.team = user.team;
            }
        });

        updateCredentials(credentials);
        setLogin(user);
    }

    else {
        const find = credentials.some(item => {
            return item.email === user.email;
        });

        if (find) {
            getAlert(form, 'Введите валидный email');

            return false;
        } else {
            setCredentials(user, credentials);
            setLogin(user);

            return user;
        }
    }
}

function getActiveBlock(prev, next) {
    prev.style.display = 'none';
    prev.classList.remove('active');
    next.style.display = 'block';
    next.classList.add('active');
}

function getFormData() {
    const user = {};
    const active = document.querySelector('.form.active');
    const isName = active.classList.contains('form--registration') ? true : false;
    const inputs = active.querySelectorAll('input:not([type="submit"]');
    const [email, pass, name = isName] = inputs;
    let fullUser = true;

    user.email = getValidity(email);
    user.password = getValidity(pass);

    if (isName) {
        user.name = getValidity(name);
    }

    for (let value in user) {
        if (user[value] === undefined) {
            fullUser = false;
            break;
        }
    }

    return fullUser ? user : false;
}

function getValidity(input) {
    const value = input.value;
    const attr = input.getAttribute('type');

    if (attr === 'email') {
        const array = Array.from(value);
        const isEmail = array.find(item => {
            return item === '@';
        });

        return isEmail ? value : getError(input, attr);
    } else if (attr === 'password') {
        const valueTrim = value.trim();

        return (valueTrim === '' || valueTrim.length < 8) ? getError(input, attr) : valueTrim;
    } else {
        const arrayName = value.split(' ');
        const lengthName = arrayName.find(item => {
            return item.length < 3;
        });

        return (value === '' || arrayName.length !== 2 || lengthName) ? getError(input, attr) : value;
    }
}

function getAlert(form, text) {
    const inputs = form.querySelectorAll('input:not([type="submit"]');
    alert(text);

    inputs.forEach(item => {
        item.value = '';
        item.style.borderColor = 'none';
    });
}
