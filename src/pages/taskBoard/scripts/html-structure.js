import {getCredentials} from '../../../api/index';

function getDropDownHTML() {
    const array = getCredentials();
    const html = array.map(item => {
        return `<div class="dropdown__list__item" data-id="${item.email}" data-name="${item.name}"><p class="form__input form__input--option" >${item.name}</p></div>`;
    }).join('');

    return html;
}

function getTagDropDownHTML() {
    const tags = ['Дизайн', 'Бекенд', 'Фронтенд'];

    const html = tags.map(item => {
        return `<div class="dropdown__list__item" data-name="${item}"><p class="form__input form__input--option" >${item}</p></div>`;
    }).join('');

    return html;
}

function getTaskHTML(item) {
    return `<a href="#" class="task task__block" data-id="${item.deadline}">
				<p class="task__description">${item.title}</p>
				<div class="flex">
					<img src="img/userpic.jpg" class="task__executor" alt="">
					<p class="task__label development">${item.tag}</p>
				</div>
				<img src="img/img_prev.jpg" class="task__img" alt="">
			</a>`;
}

function getBoarderTaskHTML(item) {
    return `<a href="#" class="task task__block" data-date=${item.date}>
					<span class="checkbox">
						<svg xmlns="http://www.w3.org/2000/svg" width="12" height="9" viewBox="0 0 12 9">
						    <path id="Path" d="M11.561.44a1.5,1.5,0,0,0-2.124,0L4.5,5.379,2.56,3.441A1.5,1.5,0,0,0,.439,5.562l3,3a1.5,1.5,0,0,0,2.121,0l6-6A1.5,1.5,0,0,0,11.561.44Z" fill="#b8b8b8" />
						</svg>
						<input type="checkbox">
					</span>
					<div>
						<p class="task__description">${item.title}</p>
						<div class="flex">
							<img src="img/userpic.jpg" class="task__executor" alt="">
							<p class="task__label development ${item.tag}">${item.tag}</p>
						</div>
					</div>
				</a>`;
}

function getCommentHTML( initial ) {
    if(Array.isArray(initial)) {
        const array = initial.map(item => {
            return `<div class="commet comment__block">
						<div class="commet__icon"><img src="img/userpic-big.jpg" alt=""></div>
						<div class="commet__text__wrapp">
							<p class="commet__name">${item.name}, <span class="commet__role">${item.role}</span></p>

							<p class="commet__text"><a href="#" class="person__tag">${item.person}</a> ${item.value}</p>

							<p class="commet__date">${item.date}</p>
						</div>
					</div>`;
        }).join('');

        return array;
    } else {
        const { name, role, person, value, date } = initial;

        return `<div class="commet comment__block">
						<div class="commet__icon">
							<img src="img/userpic-big.jpg" alt="">
						</div>
						<div class="commet__text__wrapp">
							<p class="commet__name">${name}, <span class="commet__role">${role}</span></p>

							<p class="commet__text"><a href="#" class="person__tag">${person}</a> ${value}</p>

							<p class="commet__date">${date}</p>
						</div>
					</div>`;
    }
}

export {
    getDropDownHTML,
    getTagDropDownHTML,
    getTaskHTML,
    getCommentHTML,
    getBoarderTaskHTML
};
