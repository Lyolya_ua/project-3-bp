import '../../../css/style.scss';
import { getDropDown, getError, resetError, resetValue } from '../../index/scripts/common';
import { getDropDownHTML, getTagDropDownHTML, getTaskHTML } from './html-structure';
import { getLogin, updateTasks, getProjects, setProjects } from '../../../api/index';

const task = {};
const main = document.querySelector('main');
const tabNavigation = document.querySelector('.tab__navigation');
const tabNavigationItems = tabNavigation.querySelectorAll('li');
const tabActiveItem = tabNavigation.querySelector('li[data-id="task-board"]');
const taskBoards = main.querySelectorAll('.task__column');
const addTaskBtns = main.querySelectorAll('.task__column .item__btn');
const form = main.querySelector('.form--task');
const submitBtn = form.querySelector('input[type="submit"]');
const assigneeDropDown = form.querySelector('.dropdown--assignee');
const assigneeDropDownToggle = assigneeDropDown.querySelector('.dropdown__toggle');
const assigneeDropDownList = assigneeDropDown.querySelector('.dropdown__list');
const assigneeInput = assigneeDropDown.querySelector('.form__input');
const assigneeDropDownHTML = getDropDownHTML();
const tagDropDown = form.querySelector('.dropdown--tag');
const tagDropDownToggle = tagDropDown.querySelector('.dropdown__toggle');
const tagDropDownList = tagDropDown.querySelector('.dropdown__list');
const tagInput = tagDropDown.querySelector('.form__input');
const tagDropDownHTML = getTagDropDownHTML();
const projectSection = document.querySelector('.projects.section');
const projectList = projectSection.querySelector('.projects__list');
const projectItems = projectList.querySelectorAll('li');
const projectTitle = document.querySelector('.prj__title__block h1');
const userProfile = document.querySelector('.user.user__section');
const teamSection = document.querySelector('.team.section');
const teams = teamSection.querySelectorAll('.team__description');

teams.forEach(item => {
    item.addEventListener('click', () => {
        window.location.href = 'team.html';
    })
});

userProfile.addEventListener('click', () => {
    window.location.href = 'profile-settings.html';
});

(function () {
    const userName = userProfile.querySelector('.user__name');
    const userRole = userProfile.querySelector('.user__role');
    const loginObj = getLogin();
    const listItem = projectSection.querySelector('.projects__list li');
    const dataId = listItem.getAttribute('data-id');
    const title = projectSection.querySelector('.projects__list a span').innerHTML;
    const projects = [];
    const projectsArray = getProjects();

    userName.innerHTML = loginObj.name;
    loginObj.role ? userRole.innerHTML = loginObj.role : userRole.innerHTML = '';

    if(projectsArray.length === 0) {
        projectItems.forEach(item => {
            const project = {
                dataId: item.getAttribute('data-id'),
                title: item.querySelector('a span').innerHTML,
                tasks: []
            };
            projects.push(project);
        });

        setProjects(projects);
    }

    projectTitle.innerHTML = title;
    projectTitle.setAttribute('data-id', dataId);
    listItem.classList.add('active');

    getProjectTaskItems(projectsArray, dataId)
})();

function getProjectTaskItems(array, dataId) {
    const displayedProject = array.filter(item => item.dataId === dataId);

    if(displayedProject.length > 0) {
        const tasks = displayedProject[0].tasks;

        tasks.forEach(item => {
            renderTask(item)
        });
    }
}

tabActiveItem.classList.add('active');
assigneeDropDownList.insertAdjacentHTML('afterBegin', assigneeDropDownHTML);
tagDropDownList.insertAdjacentHTML('afterBegin', tagDropDownHTML);

projectItems.forEach(item => {
    item.addEventListener('click', () => {
        const prevActiveItem = projectList.querySelector('li.active');
        const dataId = item.getAttribute('data-id');
        const title = item.querySelector('a span').innerHTML;

        projectTitle.innerHTML = title;
        projectTitle.setAttribute('data-id', dataId);
        prevActiveItem.classList.remove('active');
        item.classList.add('active');

        const projectsArray = getProjects();
        const prevTasks = main.querySelectorAll('.task.task__block');

        prevTasks.forEach(item => {
            item.remove();
        });

        getProjectTaskItems(projectsArray, dataId);
    })
});

tabNavigationItems.forEach(item => {
    item.addEventListener('click', () => {
        const dataId = item.getAttribute('data-id');

        if(dataId === 'task-board') {
            window.location.href = 'task-board.html';
        } else if(dataId === 'task-description') {
            window.location.href = 'task-description.html';
        } else if (dataId === 'calendar') {
            window.location.href = 'calendar.html';
        }
    })
});

//- ADD TASK
addTaskBtns.forEach(btn => {
    btn.addEventListener('click', () => {
        const btnDataId = btn.getAttribute('data-id');

        task.board = btnDataId;

        form.style.display = 'block';
        taskBoards.forEach(item => item.style.display = 'none' );
    });
});

submitBtn.addEventListener('click', (e) => {
    e.preventDefault();
    getTaskObj();  // add to column
});

function getTaskObj() {
    const project = projectTitle.getAttribute('data-id');
    const title = document.getElementById('taskTitle');
    const description = document.getElementById('taskDescription');
    const deadline = document.getElementById('deadline');
    let isValidate = true;

    task.project = project;
    task.title = title.value;
    task.deadline = deadline.value;
    task.description = description.value;
    task.date = new Date();
    task.comments = [];

    !task.title ? (isValidate = getError(title)) : resetError(title);
    !task.deadline ? (isValidate = getError(deadline)) : resetError(deadline);
    !task.description ? (isValidate = getError(description)) : resetError(description);
    !task.tag ? (isValidate = getError(tagDropDownToggle)) : resetError(tagDropDownToggle, true);
    !task.assignee ? (isValidate = getError(assigneeDropDownToggle)) : resetError(assigneeDropDownToggle, true);

    if(isValidate) {
        resetValue(title, description, deadline, assigneeInput, tagInput);
        updateTasks(task); // add to local storage
        renderTask(task);
        addTaskToProject(task);
        form.style.display = 'none';
        taskBoards.forEach(item => item.style.display = 'block' );

        return task;
    }
}

function renderTask(task) {
    const dataId = task.board;
    const column = main.querySelector(`div[data-id="${dataId}"] .item__header`);
    const html = getTaskHTML(task);

    column.insertAdjacentHTML('afterEnd', html);
}

function addTaskToProject(task) {
    const projectsArray = getProjects();

    const updatedProjectArray = projectsArray.map(item => {
        if(item.dataId === task.project) {
            item.tasks.push(task);
        }
        return item;
    });

    setProjects(updatedProjectArray);
}

//- DROP DOWNS
assigneeDropDown.addEventListener('click', () => {
    getDropDown(assigneeDropDown, assigneeDropDownToggle);
});

assigneeDropDownList.addEventListener('click', (e) => {
    const target = e.target;
    const isTargetClass = target.classList.contains('form__input--option');

    if (isTargetClass) {
        const dataId = target.parentNode.getAttribute('data-id');
        const name = target.parentNode.getAttribute('data-name');

        assigneeInput.placeholder = name;
        assigneeInput.value = name;
        task.assignee = dataId;
        task.name = name;
    } else {
        const dataId = target.getAttribute('data-id');
        const name = target.getAttribute('data-name');

        assigneeInput.placeholder = name;
        assigneeInput.value = name;
        task.assignee = dataId;
        task.name = name;
    }
});

tagDropDown.addEventListener('click', () => {
    getDropDown(tagDropDown, tagDropDownToggle);
});

tagDropDownList.addEventListener('click', (e) => {
    const target = e.target;
    const isTargetClass = target.classList.contains('form__input--option');
    const tag = isTargetClass ? target.innerHTML : target.getAttribute('data-name');
    const prevLabel = tagDropDown.querySelector('p.label');
    const label = document.createElement('p');

    if(prevLabel) {
        prevLabel.remove();
    }

    label.classList.add('label');
    label.innerHTML = tag;
    tagInput.value = tag;
    task.tag = tag;
    tagInput.after(label);
});
