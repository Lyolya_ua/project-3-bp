import '../../../css/style.scss';
import {getLogin, setLogin, getCredentials, updateCredentials} from '../../../api/index';
import {getError, resetError} from "../../index/scripts/common";
import {getTeamNameHTML} from "./html-structure";

const projectSection = document.querySelector('.projects.section');
const projectList = projectSection.querySelector('.projects__list');
const projectItems = projectList.querySelectorAll('li');
const form = document.querySelector('.form--profile');
const userInput = document.getElementById('user');
const roleInput = document.getElementById('role');
const teamInput = document.getElementById('team');
const aboutArea = document.getElementById('about');
const submit = form.querySelector('.submit__btn');
const cancel = form.querySelector('.cancel__btn');
const teamWrap = form.querySelector('.team__wrapp');
const userProfile = document.querySelector('.user.user__section');
const userName = userProfile.querySelector('.user__name');
const userRole = userProfile.querySelector('.user__role');
const teamSection = document.querySelector('.team.section');
const teams = teamSection.querySelectorAll('.team__description');
const credentialsData = getCredentials();
const loginData = getLogin();

(function () {
    userName.innerHTML = loginData.name;
    loginData.role ? userRole.innerHTML = loginData.role : userRole.innerHTML = '';

    resetProfile();
})();

teams.forEach(item => {
    item.addEventListener('click', () => {
        window.location.href = 'team.html';
    })
});

projectItems.forEach(item => {
    item.addEventListener('click', () => {
        window.location.href = 'task-board.html';
    })
});

submit.addEventListener('click', (e) => {
    e.preventDefault();
    updateProfile();
});

cancel.addEventListener('click', (e) => {
    e.preventDefault();
    resetProfile();
});

function updateProfile() {
    const name = getValidity(userInput);
    const role = getValidity(roleInput);
    const about = getValidity(aboutArea);
    const team = teamInput.value;

    if (name) {
        loginData.name = name;
        userName.innerHTML = name;
        resetError(userInput);
    }

    if (role) {
        loginData.role = role;
        userRole.innerHTML = role;
        resetError(roleInput);
    }

    if (about || about === '') {
        loginData.about = about;
        resetError(aboutArea);
    }

    if (team) {
        const html = getTeamNameHTML(team);
        loginData.team = team;

        resetError(teamWrap);

        if (html) {
            const prevItem = teamWrap.querySelector('.team__name');
            prevItem ? prevItem.remove() : null;
            teamWrap.insertAdjacentHTML('afterBegin', html);

            const close = teamWrap.querySelector('.team__name');

            close.addEventListener('click', () => {
                close.remove();
                teamInput.value = '';
                loginData.team = '';
            });
        } else {
            loginData.team = '';
            teamInput.value = '';
            teamInput.placeholder = '';
            getError(teamWrap);
        }
    } else {
        loginData.team = team;
    }

    setLogin(loginData);
    const updatedCredentials = updateObjects();

    updateCredentials(updatedCredentials);
    console.log(localStorage.login)
}

function resetProfile() {
    userInput.value = loginData.name;
    aboutArea.value = loginData.about ? loginData.about : loginData.about = '';

    if (loginData.role) {
        roleInput.placeholder = loginData.role;
        roleInput.value = loginData.role;
    } else {
        roleInput.placeholder = '';
        roleInput.value = '';
    }

    if (loginData.team) {
        teamInput.value = loginData.team;
    }

    if (loginData.team) {
        const html = getTeamNameHTML(loginData.team);
        teamInput.value = loginData.team;

        if (html) {
            const prevItem = teamWrap.querySelector('.team__name');
            prevItem ? prevItem.remove() : null;
            teamWrap.insertAdjacentHTML('afterBegin', html);

            const close = teamWrap.querySelector('.team__name');

            close.addEventListener('click', () => {
                close.remove();

                teamInput.value = '';
                teamInput.placeholder = '';
            })
        }
    }
}

function getValidity(input) {
    const value = input.value;
    const attr = input.getAttribute('id');

    if (attr === 'user') {
        const trimValue = value.trim();
        const arrayName = trimValue.split(' ');
        const lengthName = arrayName.find(item => {
            return item.length < 3;
        });

        return (value === '' || arrayName.length !== 2 || lengthName) ? getError(input, attr) : value;
    } else if (attr === 'role') {
        const valueTrim = value.trim();

        return (valueTrim.length > 20) ? getError(input, attr) : valueTrim;
    } else if (attr === 'about') {
        return (value.length > 500) ? getError(input, attr) : value === '' ? value : value;
    }
}

function updateObjects() {
    const array = credentialsData.map(item => {
        if (loginData.email === item.email) {
            Object.assign(item, loginData);
        }

        return item;
    });

    return array;
}
