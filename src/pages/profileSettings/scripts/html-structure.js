function getTeamNameHTML(team) {
    const newTeam = team.toLocaleLowerCase();
    const value =
        newTeam === 'design' && 'дизайн' ? 'Дизайн' :
        newTeam === 'frontend' && 'фронтенд' ? 'Фронтенд' :
        newTeam === 'backend' && 'бекенд' ? 'Бекенд' : null;

    return value ? `<div class="team__name">${value} <img src="img/icon/close.svg" class="close__icon" alt=""></div>` : false;
}

export { getTeamNameHTML };
