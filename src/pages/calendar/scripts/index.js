import '../../../css/style.scss';
import {getLogin} from "../../../api";

const tabNavigation = document.querySelector('.tab__navigation');
const tabNavigationItems = tabNavigation.querySelectorAll('li');
const tabActiveItem = tabNavigation.querySelector('li[data-id="calendar"]');
const projectSection = document.querySelector('.projects.section');
const projectList = projectSection.querySelector('.projects__list');
const projectItems = projectList.querySelectorAll('li');
const userProfile = document.querySelector('.user.user__section');
const teamSection = document.querySelector('.team.section');
const teams = teamSection.querySelectorAll('.team__description');

(function () {
    const userName = userProfile.querySelector('.user__name');
    const userRole = userProfile.querySelector('.user__role');
    const loginObj = getLogin();

    userName.innerHTML = loginObj.name;
    loginObj.role ? userRole.innerHTML = loginObj.role : userRole.innerHTML = '';
})();

teams.forEach(item => {
    item.addEventListener('click', () => {
        window.location.href = 'team.html';
    })
});

userProfile.addEventListener('click', () => {
    window.location.href = 'profile-settings.html';
});

tabActiveItem.classList.add('active');

tabNavigationItems.forEach(item => {
    item.addEventListener('click', () => {
        const dataId = item.getAttribute('data-id');

        if(dataId === 'task-board') {
            window.location.href = 'task-board.html';
        } else if(dataId === 'task-description') {
            window.location.href = 'task-description.html';
        } else if (dataId === 'calendar') {
            window.location.href = 'calendar.html';
        }
    })
});

projectItems.forEach(item => {
    item.addEventListener('click', () => {
        window.location.href = 'task-board.html';
    })
});
