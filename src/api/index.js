/**///- CREDENTIALS
function getCredentials() {
    return JSON.parse(localStorage.getItem('credentials')) || [];
}

function setCredentials(user, credentials) {
    localStorage.setItem('credentials', JSON.stringify([user, ...credentials]));
}

function updateCredentials(credentials) {
    localStorage.setItem('credentials', JSON.stringify(credentials));
}

//- LOGIN
function getLogin() {
    return JSON.parse(localStorage.getItem('login'));
}

function setLogin(user) {
    localStorage.setItem('login', JSON.stringify(user));
}

//-TASKS
function getTasks() {
    return JSON.parse(localStorage.getItem('tasks')) || [];
}

function setTasks(tasks) {
    localStorage.setItem('tasks', JSON.stringify(tasks));
}

function updateTasks(task) {
    const tasks = JSON.parse(localStorage.getItem('tasks')) || [];
    tasks.push(task);

    localStorage.setItem('tasks', JSON.stringify(tasks)) ;
}

//-PROJECTS
function getProjects() {
    return JSON.parse(localStorage.getItem('projects')) || [];
}

function setProjects(projects) {
    localStorage.setItem('projects', JSON.stringify(projects));
}

export {
    getCredentials,
    setCredentials,
    updateCredentials,
    getLogin,
    setLogin,
    getTasks,
    setTasks,
    updateTasks,
    getProjects,
    setProjects
};
